<link href="styles/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" href="styles/css3splitmenu.css" />
<link rel="stylesheet" href="styles/sidemenuflip.css" />
<link rel="stylesheet" href="styles/pagination.css" />
<link rel="stylesheet" href="styles/csspushdown.css" />

<script type="text/javascript">
// (c) 2001 Douglas Crockford
// 2001 June 3
// The -is- object is used to identify the browser. Every browser edition
// identifies itself, but there is no standard way of doing it, and some of
// the identification is deceptive. This is because the authors of web
// browsers are liars. For example, Microsoft's IE browsers claim to be
// Mozilla 4. Netscape 6 claims to be version 5.
var is = {
ie: navigator.appName == 'Microsoft Internet Explorer',
java: navigator.javaEnabled(),
ns: navigator.appName == 'Netscape',
ua: navigator.userAgent.toLowerCase(),
version: parseFloat(navigator.appVersion.substr(21)) ||
parseFloat(navigator.appVersion),
win: navigator.platform == 'Win32'
}
is.mac = is.ua.indexOf('mac') >= 0;
if (is.ua.indexOf('opera') >= 0) {
is.ie = is.ns = false;
is.opera = true;
alert('MAC');
}
if (is.ua.indexOf('gecko') >= 0) {
is.ie = is.ns = false;
is.gecko = true;

}
if (is.ua.indexOf('mozilla') >= 0) {
is.ie = is.ns = false;
is.gecko = false;
document.writeln(navigator.userAgent.toLowerCase());
}

</script>


<div class="css3splitmenu" style="margin-left:0px; margin-top:7px;">
<a href="userdetails.php?action=view&alert_msg=View%20Account!">Welcome: <u><?php echo "LOGIN"; ?></u></a> <input type="checkbox" />

<ul id="t">
<form>
<li>Username:</li>
<li><input type="text" /></li>
<li>Password:</li>
<li><input type="text" /></li>
<li><input type="reset" /><input type="submit" value="Login" /></li>
</form>
</ul>
</div>

<!-- Script below should follow all split menus -->

<script type="text/javascript">

( function(){ // local scope

	if (!document.querySelectorAll || !document.addEventListener)
		return
	var uls = document.querySelectorAll('div.css3splitmenu > ul'),
			docbody = document.documentElement || document.body,
			checkboxes = document.querySelectorAll('div.css3splitmenu > input[type="checkbox"]'),
			zindexvalue = 100

	for (var i=0; i<uls.length; i++){
		( function(x){ // closure to capture each i value
			checkboxes<i>.addEventListener('click', function(e){
				uls[x].style.zIndex = ++zindexvalue
				for (var y=0; y<uls.length; y++){ // loop through checkboxes other than current and uncheck them (since Chrome doesn't respond to onblur event on checkboxes)
					if (y != x)
						checkboxes[y].checked = false
				}
				e.cancelBubble = true
			})
			checkboxes<i>.addEventListener('blur', function(e){
				setTimeout(function(){checkboxes[x].checked = false}, 100) // delay before menu closes, for Opera's sake (otherwise links are un-navigatable)
				e.cancelBubble = true
			})
		}) (i)
	}

	docbody.addEventListener('click', function(e){
		for (var i=0; i<uls.length; i++){
			checkboxes<i>.checked = false
		}
	})

})();


</script>

